package main

import (
	"fmt"
	"image/jpeg"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/gen2brain/go-fitz"
)

// header of the final markdown file.
var rawHeader = []byte(`# %s

---
`)

// page element of the final markdown file.
// will be used for each page of the pdf file
var rawPageEntry = []byte(`## Page %d
![%s](%s)
### Notes



---
`)

// main function
func main() {

	if len(os.Args) < 2 || os.Args[1] == "" {
		log.Println("You need to pass the filename as parameter")
		os.Exit(1)
	}

	filename := os.Args[1]
	fileBaseName := BaseName(filename)

	// Final markdown string, which will be parsed into .md file
	finalMarkdown := ""

	// Open pdf
	doc, err := fitz.New(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer doc.Close()

	// Create directory
	err = os.Mkdir(fileBaseName, 0750)
	if err != nil && !os.IsExist(err) {
		log.Fatal(err)
	}

	// Create markdown file
	f, err := os.Create(fileBaseName + ".md")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	// Append header to final markdown string
	formattedHeader := fmt.Sprintf(string(rawHeader), filename)
	finalMarkdown += formattedHeader

	// Extract pages as images
	for n := 0; n < doc.NumPage(); n++ {
		img, err := doc.Image(n)
		if err != nil {
			log.Fatal(err)
		}
		filePath := filepath.Join(fileBaseName, fmt.Sprintf("%s%03d.jpg", fileBaseName, n))
		f, err := os.Create(filePath)
		if err != nil {
			log.Fatal(err)
		}

		err = jpeg.Encode(f, img, &jpeg.Options{Quality: jpeg.DefaultQuality})
		if err != nil {
			log.Fatal(err)
		}

		// Append page to final markdown string
		formattedPageEntry := fmt.Sprintf(string(rawPageEntry), n+1, f.Name(), filePath)
		finalMarkdown += formattedPageEntry

		f.Close()
	}

	// Write contents into markdown file
	_, err = f.WriteString(finalMarkdown)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Created " + fileBaseName + ".md")
}

func BaseName(s string) string {
	n := strings.LastIndexByte(s, '.')
	if n == -1 {
		return s
	}
	return s[:n]
}
