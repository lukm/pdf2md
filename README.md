# pdf2md
**TLDR**: pdf2md is a small binary written in go, which takes a pdf file (typically presentation slides)
and creates a subfolder with a jpg file for each slide and a `filename.md` file, which
renders all these images with additional space between each slides for notes.

One use case is taking the professors slides of a university course and create a markdown file
quickly to follow along and type notes, without managing separate files.

## Example
`test.pdf` is a pdf with 2 pages in this exaple.

Command: `pdf2md test.pdf`

Outcome of files in directory containing `test.pdf`
```
.
├── test.pdf
├── test.md
├── test
│  ├── test000.jpg
│  └── test001.jpg

```
test.md:
```markdown
# test.pdf

---
## Page 1
![test/test000.jpg](test/test000.jpg)
### Notes



---
## Page 2
![test/test001.jpg](test/test001.jpg)
### Notes



---

```

## Installation (Linux)
* Download the binary of the newest release from [https://codeberg.org/lukm/pdf2md/releases](https://codeberg.org/lukm/pdf2md/releases)
* `chmod +x ./pdf2md`
* `sudo mv ./pdf2md /usr/local/bin/pdf2md`

## Building from source
```shell
go get
go build .
```


## License

lukm/pdf2md is [GNU GPLv3 licensed](./LICENSE)